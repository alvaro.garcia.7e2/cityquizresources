package cat.itb.cityquiz.domain;

import java.util.List;

public class Question {
    City correctCity;
    List<City> possibleCities;

    public Question(City correctCity, List<City> possibleCities) {
        this.correctCity = correctCity;
        this.possibleCities = possibleCities;
    }

    public City getCorrectCity() {
        return correctCity;
    }

    public List<City> getPossibleCities() {
        return possibleCities;
    }
}
