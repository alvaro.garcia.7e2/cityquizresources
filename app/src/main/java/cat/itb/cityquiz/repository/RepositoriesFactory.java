package cat.itb.cityquiz.repository;

import cat.itb.cityquiz.data.citiyfiledata.CitiesFileSource;

public class RepositoriesFactory {
    private static final CitiesFileSource citiesFileSource = new CitiesFileSource();
    private static final GameLogic gameLogic = new GameLogic(citiesFileSource);

    public static GameLogic getGameLogic() {
        return gameLogic;
    }
}
