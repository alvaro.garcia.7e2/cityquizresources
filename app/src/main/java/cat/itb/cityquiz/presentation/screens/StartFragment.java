package cat.itb.cityquiz.presentation.screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class StartFragment extends Fragment {

    private GameViewModel mViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);

        mViewModel.getGame().observe(this, this::onGameChangued);

    }

    private void onGameChangued(Game game) {
        if (game == null) mViewModel.startGame();

        else mViewModel.getGame();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // recordar cojer las vistas del butternkive(botones, textxViews...)
        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.start_button)
    public void onViewClicked() {
        // no hay que pasar una action porque no necesitamos parametros, directamente le decimos el nombre de la flecha del nav_graph
        Navigation.findNavController(getView()).navigate(R.id.action_startgame);
    }
}
