package cat.itb.cityquiz.presentation.screens;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class GameFragment extends Fragment {

    @BindView(R.id.imageGame)
    ImageView imageGame;
    @BindView(R.id.cityGame1)
    Button cityGame1;
    @BindView(R.id.cityGame2)
    Button cityGame2;
    @BindView(R.id.cityGame4)
    Button cityGame4;
    @BindView(R.id.cityGame5)
    Button cityGame5;
    @BindView(R.id.cityGame6)
    Button cityGame6;
    @BindView(R.id.cityGame3)
    Button cityGame3;
    private GameViewModel mViewModel;


    public static GameFragment newInstance() {
        return new GameFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(GameViewModel.class);

        LiveData<Game> game = mViewModel.getGame();
        Question question = game.getValue().getCurrentQuestion();

        if (mViewModel.getGame().getValue().isFinished()){
            viewScoreFragment();
        } else {
            displayGame(question);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    public void viewScoreFragment(){
        Navigation.findNavController(getView()).navigate(R.id.scoreFragment);
    }
    private void displayImage(Question question, Context appContext){
        String photoName = ImagesDownloader.scapeName(question.getPossibleCities().get(0).getName());
        int resID = appContext.getResources().getIdentifier(photoName, "drawable", appContext.getPackageName());
        imageGame.setImageResource(resID);
    }
    private void displayTextBtn(Question question){
        cityGame1.setText(question.getPossibleCities().get(0).getName());
        cityGame2.setText(question.getPossibleCities().get(1).getName());
        cityGame3.setText(question.getPossibleCities().get(2).getName());
        cityGame4.setText(question.getPossibleCities().get(3).getName());
        cityGame5.setText(question.getPossibleCities().get(4).getName());
        cityGame6.setText(question.getPossibleCities().get(5).getName());
    }
    private void displayGame(Question question){
        displayImage(question, imageGame.getContext());
        displayTextBtn(question);
    }

    @OnClick({R.id.cityGame1, R.id.cityGame2, R.id.cityGame4, R.id.cityGame5, R.id.cityGame6, R.id.cityGame3})
    public void onViewClicked(View view) {
        LiveData<Game> game = mViewModel.getGame();
        Game g = game.getValue();

        switch (view.getId()) {
            case R.id.cityGame1:
                answerQuestion(g, 0);
                break;
            case R.id.cityGame2:
                answerQuestion(g, 1);
                break;
            case R.id.cityGame4:
                answerQuestion(g, 3);
                break;
            case R.id.cityGame5:
                answerQuestion(g, 4);
                break;
            case R.id.cityGame6:
                answerQuestion(g, 5);
                break;
            case R.id.cityGame3:
                answerQuestion(g, 2);
                break;
        }
    }

    private void answerQuestion(Game game, int position) {
        Question question = game.getCurrentQuestion();
        if (game.isFinished()){
            viewScoreFragment();
        } else {
            mViewModel.answerQuestion(position);
            game.getScore();
            displayGame(question);
        }
    }
}
