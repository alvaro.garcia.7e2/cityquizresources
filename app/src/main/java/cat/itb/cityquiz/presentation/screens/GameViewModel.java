package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;


public class GameViewModel extends ViewModel {
    MutableLiveData<Game> game = new MutableLiveData<>();
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();

    public void startGame(){
        gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
    }

    public void answerQuestion(int position){
        Game g = gameLogic.answerQuestions(game.getValue(), position);
        game.postValue(g);
    }

    public MutableLiveData<Game> getGame(){
        return game;
    }

}
