package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.screens.StartFragment;

import static androidx.navigation.Navigation.findNavController;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);
        // Afegir si es vol mostrar el 'back' a la actionBar
        NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_host_fragment));
    }
    @Override
    public boolean onSupportNavigateUp() {
        // Afegir si es vol mostrar el 'back' a la actionBar
        return findNavController(this, R.id.nav_host_fragment).navigateUp();

    }
}
